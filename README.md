Role Name
=========

A role that installs the free version of Rstudio Server 

Role Variables
--------------

The most important variables are listed below:

``` yaml
rstudio_install_server: True
rstudio_enabled: '{{ rstudio_install_server }}'
rstudio_server_version: '1.4.1106'
rstudio_file: 'rstudio-server-{{ rstudio_server_version }}-amd64.deb'
rstudio_download_url: 'https://download2.rstudio.org/server/bionic/amd64/{{ rstudio_file }}'

rstudio_install_kill_script: True
# cron job minutes
rstudio_kill_script_frequency: "*/5"

r_session_timeout_minutes: 360
r_session_disconnected_timeout_minutes: 360
r_session_quit_child_processes_on_exit: 0
r_session_default_working_dir: '~'
r_session_default_new_project_dir: '~'
r_session_save_action_default: 'yes'
r_session_allow_shell: 1
r_session_allow_terminal_websockets: 1
r_session_limit_cpu_time_minutes: 0
r_session_limit_file_upload_size_mb: 0
r_session_limit_xfs_quota: 'no'

rstudio_rserver_conf_opts:
  - { name: 'r-cran-repos', value: 'http://cran.mirror.garr.it/mirrors/CRAN/' }
  - { name: 'session-timeout-minutes', value: '{{ r_session_timeout_minutes }}' }
  - { name: 'session-disconnected-timeout-minutes', value: '{{ r_session_disconnected_timeout_minutes }}' }
  - { name: 'session-quit-child-processes-on-exit', value: '{{ r_session_quit_child_processes_on_exit }}' }
  - { name: 'session-default-working-dir', value: '{{ r_session_default_working_dir }}' }
  - { name: 'session-default-new-project-dir', value: '{{ r_session_default_new_project_dir }}' }
  - { name: 'session-save-action-default', value: '{{ r_session_save_action_default}}' }
  - { name: 'allow-shell', value: '{{ r_session_allow_shell }}' }
  - { name: 'allow-terminal-websockets', value: '{{ r_session_allow_terminal_websockets }}' }
  - { name: 'limit-cpu-time-minutes', value: '{{ r_session_limit_cpu_time_minutes }}' }
  - { name: 'limit-file-upload-size-mb', value: '{{ r_session_limit_file_upload_size_mb }}' }
  - { name: 'limit-xfs-disk-quota', value: '{{ r_session_limit_xfs_quota }}' }

rstudio_rsession_conf_opts:
  - { name: 'www-address', value: '0.0.0.0' }
```

Dependencies
------------

None

License
-------

EUPL-1.2

Author Information
------------------

Andrea Dell'Amico, <andrea.dellamico@isti.cnr.it>
